# Environment docker for Drupal

## Installation docker

- Create directory .database/drupal for initializer database
```bash
mkdir .database/drupal
```

- Build docker
```bash
docker-compose build
```

- Up docker
```bash
docker-compose up -d
```

- Enter in container drupal
```bash
docker-compose exec -u www-data drupal bash
```

- Install vendor
```bash
composer install
```

- Stop docker
```bash
docker-compose down -v
```

- Create dump
```bash
docker-compose exec -u www-data drupal vendor/bin/drush sql-dump --structure-tables-list=cache,cache_*,search_*,sessions,watchdog > .database/dump/filename.sql
```
Privileged name : AAAA-MM-DD-HH.sql